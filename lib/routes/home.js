// --------------------------------------------------------------------------------------------------------------------

'use strict'

// local
var pkg = require('../../package.json')
var env = require('../env.js')
var db = require('../db.js')
var stats = require('../stats.js')

// --------------------------------------------------------------------------------------------------------------------

module.exports = function (app) {
  app.get('/', function (req, res, next) {
    stats.home.inc()
    res.render('index', {
      title: pkg.title
    })
  })

  app.post('/b/', function (req, res, next) {
    stats.createBin.inc()
    // since someone is posting here, let's create a bin and redirect to it
    db.createBin(function (err, bin) {
      if (err) return next(err)
      res.redirect('/b/' + bin.binId)
    })
  })

  var pages = [
    '/',
    '/api/'
  ]
  var sitemap = pages.map(function (page) {
    return env.baseUrl + page + '\n'
  }).join('')

  app.get('/sitemap.txt', function (req, res) {
    res.set('Content-Type', 'text/plain')
    res.send(sitemap)
  })

  app.get(
    '/stats',
    (req, res, next) => {
      var finished = false
      var got = 0
      var currentStats = {}

      // get some bits
      stats.events.forEach((eventName) => {
        stats[eventName].values((err, data) => {
          if (finished) return
          if (err) {
            finished = true
            return next(err)
          }

          got += 1

          // save this hit
          data.forEach((hit) => {
            currentStats[hit.ts] = currentStats[hit.ts] || {}
            currentStats[hit.ts][eventName] = hit.val
          })

          // if we've got all the results, render the page
          if (got === stats.events.length) {
            finished = true
            res.render('stats', {
              title: 'stats',
              events: stats.events,
              stats: currentStats
            })
          }
        })
      })
    }
  )
}

// --------------------------------------------------------------------------------------------------------------------

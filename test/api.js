// --------------------------------------------------------------------------------------------------------------------

'use strict'

// npm
const test = require('tape')

// local
const db = require('../lib/db.js')

// --------------------------------------------------------------------------------------------------------------------

test('creating an api key', function (t) {
  t.plan(2)

  const email = 'bob.jones@example.com'

  // firstly, make a bin
  db.createApi('bob.jones@example.com', function (err, val) {
    t.equal(err, null, 'There was no error when creating the API Key')
    t.equal(val.email, email, 'The email address is the same')

    t.end()
  })
})

// --------------------------------------------------------------------------------------------------------------------

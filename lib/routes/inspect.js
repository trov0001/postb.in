// --------------------------------------------------------------------------------------------------------------------

'use strict'

// local
var db = require('../db.js')
var stats = require('../stats.js')

// --------------------------------------------------------------------------------------------------------------------

module.exports = function (app) {
  // redirect to the non-slash version
  app.get('/b/:binId/', function (req, res) {
    res.redirect('/b/' + req.params.binId)
  })

  // just show the binId info
  app.get('/b/:binId', function (req, res, next) {
    stats.inspect.inc()
    db.getBin(req.params.binId, function (errGetBin, bin) {
      if (errGetBin) return next(errGetBin)

      if (!bin) {
        return res.status(404).send('404 - Not Found\n')
      }

      // let's get all of the requests in this bin
      db.getReqsFor(req.params.binId, function (errGetReqsFor, reqs) {
        if (errGetReqsFor) return next(errGetReqsFor)
        res.render('bin', { bin: bin, reqs: reqs })
      })
    })
  })
}

// --------------------------------------------------------------------------------------------------------------------
